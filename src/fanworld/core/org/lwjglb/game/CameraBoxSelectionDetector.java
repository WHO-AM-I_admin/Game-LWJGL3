package fanworld.core.org.lwjglb.game;

import java.util.Collection;
import java.util.List;

import org.joml.Intersectionf;
import org.joml.Vector2f;
import org.joml.Vector3f;

import fanworld.core.org.lwjglb.engine.graph.CoreCamera;
import fanworld.core.org.lwjglb.engine.items.GameItem;

public class CameraBoxSelectionDetector {

    private final Vector3f max;

    private final Vector3f min;

    private final Vector2f nearFar;

    private Vector3f dir;

    public CameraBoxSelectionDetector() {
        dir = new Vector3f();
        min = new Vector3f();
        max = new Vector3f();
        nearFar = new Vector2f();
    }

    public GameItem selectGameItem(Collection<? extends GameItem> gameItems, CoreCamera coreCamera) {        
        dir = coreCamera.getViewMatrix().positiveZ(dir).negate();
        List<GameItem> filteredItems = DummyGame.INSTANCE.getRenderer().getFilteredItems();
        return selectGameItem(filteredItems, coreCamera.getPosition(), dir);
    	//return null;
    }
    
    protected GameItem selectGameItem(Collection<? extends GameItem> gameItems, Vector3f center, Vector3f dir) {
        //boolean selected = false;
        GameItem selectedGameItem = null;
        float closestDistance = Float.POSITIVE_INFINITY;

        for (GameItem gameItem : gameItems) {
        	if(gameItem==null||(!gameItem.isDisplay()))continue;
            gameItem.setSelected(false);
            min.set(gameItem.getPosition());
            max.set(gameItem.getPosition());
            min.add(-gameItem.getScale(), -gameItem.getScale(), -gameItem.getScale());
            max.add(gameItem.getScale(), gameItem.getScale(), gameItem.getScale());
            //cout.p("far:"+nearFar);
            if (Intersectionf.intersectRayAab(center, dir, min, max, nearFar) && nearFar.x < closestDistance) {
                closestDistance = nearFar.x;
                selectedGameItem = gameItem;
            }
        }

        if (selectedGameItem != null) {
            selectedGameItem.setSelected(true);
            //selected = true;
        }
        return selectedGameItem;
    }
}
