package fanworld.core.org.lwjglb.engine.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.joml.FrustumIntersection;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import fanworld.core.org.lwjglb.engine.items.GameItem;
import fanworld.game.world.block.blockdata.BlockType;

public class FrustumCullingFilter {

    private final Matrix4f prjViewMatrix;

    private final FrustumIntersection frustumInt;

    public FrustumCullingFilter() {
        prjViewMatrix = new Matrix4f();
        frustumInt = new FrustumIntersection();
    }

    public void updateFrustum(Matrix4f projMatrix, Matrix4f viewMatrix) {
        // Calculate projection view matrix
        prjViewMatrix.set(projMatrix);
        prjViewMatrix.mul(viewMatrix);
        // Update frustum intersection class
        frustumInt.set(prjViewMatrix);
    }

    public void filter(Map<? extends Mesh, List<GameItem>> mapMesh) {
    	List<GameItem> entry = new ArrayList<>();
    	/*
        Vector3f pos=game.getCoreCamera().getPosition();
        World world=game.getGame().getGameClient().getCraftWorld();
        Location loc=LocationUtil.toLocation(world, pos);
        Chunk chunk=world.getChunk(loc);
        List<GameItem> entry = new ArrayList<>();
        for (Block block : chunk.getAllBlock()) {//添加游戏内物品
        	//System.out.println("test:"+gameItem.isDisplay());
        	GameItem gameItem=block.getGameItem();
            	entry.add(gameItem);
        }*/
        
    	for(BlockType type:BlockType.values()) {
    		if(type.getBlockTypeData().getMesh()!=null)
            filter(entry, type.getBlockTypeData().getMesh().getBoundingRadius());
    	}
        
        /*
        for (Map.Entry<? extends Mesh, List<GameItem>> entry : mapMesh.entrySet()) {
            List<GameItem> gameItems = entry.getValue();
            filter(gameItems, BlockType.GRASS.getBlockTypeData().getMesh().getBoundingRadius());
        }*/
    }

    public void filter(List<GameItem> gameItems, float meshBoundingRadius) {
        for (GameItem gameItem : gameItems) {
        	setInsideFrustum(gameItem,meshBoundingRadius);
        }
    }

    public boolean insideFrustum(float x0, float y0, float z0, float boundingRadius) {
        return frustumInt.testSphere(x0, y0, z0, boundingRadius);
    }
    
    public void setInsideFrustum(GameItem gameItem,float meshBoundingRadius) {
    	float boundingRadius;
        Vector3f pos;
    	if (!gameItem.isDisableFrustumCulling()) {
            boundingRadius = gameItem.getScale() * meshBoundingRadius;
            pos = gameItem.getPosition();
            gameItem.setInsideFrustum(insideFrustum(pos.x, pos.y, pos.z, boundingRadius));
        }
    }
}
