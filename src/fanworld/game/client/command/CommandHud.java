package fanworld.game.client.command;

import fanworld.game.client.hud.BasicHud;
import fanworld.game.client.hud.TextHud;

/** 
* @author byxiaobai
* 指令窗口
*/
public class CommandHud extends BasicHud implements TextHud{
	private String text="";

	@Override
	public void update() {
		
	}

	@Override
	public String getText() {
		return this.text;
	}

	@Override
	public void setText(String text) {
		this.text=text;
	}

	@Override
	public float getSize() {
		return 27;
	}

	@Override
	public float getX() {
		return 1275;
	}

	@Override
	public float getY() {
		return 125;
	}
}
