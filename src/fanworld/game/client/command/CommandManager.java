package fanworld.game.client.command;

import java.util.HashMap;

import fanworld.game.client.command.instances.*;
import fanworld.game.client.hud.HudManager;

/** 
* @author byxiaobai
* 类说明 
*/
public class CommandManager {
	public static CommandManager INSTANCE=new CommandManager();
	/**
	 * 储存指令
	 */
	private HashMap<String,CommandInstance> commandMap=new HashMap<>();
	private ReviewHud reviewHud;
	private boolean isUsingCommand=false;
	public void init() {
		reviewHud=new ReviewHud();
		HudManager.INSTANCE.addTextHud("reviewHud", reviewHud);
		//注册指令
		registerCommand("tp",new TeleportCommand());//传送
	}
	public void run(String cmd) {
		reviewHud.setText("running:"+cmd);
		String[] args=cmd.split(" ");
		CommandInstance instance=commandMap.get(args[0].toUpperCase());
		if(instance!=null)instance.run(args);
	}
	public void sendMessage(String message) {
		reviewHud.setText("message:"+message);
	}
	/**
	 * 注册指令
	 */
	public void registerCommand(String commandName,CommandInstance commandInstance) {
		commandMap.put(commandName.toUpperCase(), commandInstance);
	}
	public void setUsingCommand(boolean value) {
		isUsingCommand=value;
	}
	public boolean isUsingCommand() {
		return this.isUsingCommand;
	}
}
