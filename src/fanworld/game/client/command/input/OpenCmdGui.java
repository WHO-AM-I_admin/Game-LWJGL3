package fanworld.game.client.command.input;

import fanworld.game.client.GameData;
import fanworld.game.client.command.CommandManager;
import fanworld.game.client.input.keyboard.KeyBoard;
import fanworld.game.client.input.keyboard.KeyBoardManager;

/** 
* @author byxiaobai
* 开启指令窗口
*/
public class OpenCmdGui extends KeyBoard{
	public void init(int key) {
		KeyBoardManager.INSTANCE.registerKey(key, this);
	}

	@Override
	public void handle() {
		if(GameData.INSTANCE.isCmdGuiOpen)return;
		CommandManager.INSTANCE.setUsingCommand(true);
		GameData.INSTANCE.isCmdGuiOpen=true;
		GameData.INSTANCE.cmdGuiOpenTime=System.currentTimeMillis();
	}
}
