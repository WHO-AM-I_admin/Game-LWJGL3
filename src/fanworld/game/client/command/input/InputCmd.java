package fanworld.game.client.command.input;

import fanworld.game.client.GameData;
import fanworld.game.client.command.CommandHud;
import fanworld.game.client.hud.HudManager;
import fanworld.game.client.input.keyboard.KeyBoard;
import fanworld.game.client.input.keyboard.KeyBoardManager;
import fanworld.game.client.input.keyboard.KeyBoardUtil;
import fanworld.game.client.input.keyboard.KeyType;

/** 
* @author byxiaobai
* 类说明 
*/
public class InputCmd extends KeyBoard{

	private String lastKey;
	@Override
	public void handle() {
		if(!GameData.INSTANCE.isCmdGuiOpen)return;
		
		String str=KeyBoardUtil.getNowInputChar();
		if(str.equals(lastKey))return;
		lastKey=str;
		CommandHud cmdHud=(CommandHud)HudManager.INSTANCE.getHud("Cmd");
		if(str=="BACK") {
			int length=cmdHud.getText().length()-1;
			if(length==-1)return;
			String text=cmdHud.getText().substring(0, cmdHud.getText().length()-1);
			cmdHud.setText(text);
			return;
		}
		cmdHud.setText(cmdHud.getText()+str);
	}
	
	public void init() {
		KeyBoardManager.INSTANCE.registerKey(KeyType.CHAR.getValue(), this);
	}
}
