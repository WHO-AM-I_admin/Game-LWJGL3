package fanworld.game.client.command.instances;

import fanworld.core.org.lwjglb.game.DummyGame;
import fanworld.game.client.command.CommandInstance;
import fanworld.game.client.command.CommandManager;

/** 
* @author byxiaobai
* 类说明 
*/
public class TeleportCommand implements CommandInstance{

	@Override
	public void run(String[] args) {
		if(args.length==4) {
			CommandManager.INSTANCE.sendMessage("Teleport -- Successful");
			int x,y,z;
			x=Integer.parseInt(args[1]);
			y=Integer.parseInt(args[2]);
			z=Integer.parseInt(args[3]);
			DummyGame.INSTANCE.getCoreCamera().setPosition(x, y, z);
		}else {
			CommandManager.INSTANCE.sendMessage("Wrong Location");
		}
	}

}
