package fanworld.game.client.hud;
/** 
* @author byxiaobai
* 类说明 
*/
public interface TextHud extends Hud{
	public String getText();
	public void setText(String text);
	public float getSize();
	public float getX();
	public float getY();
}
