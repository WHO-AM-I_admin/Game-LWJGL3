package fanworld.game.client.hud;

import java.util.HashMap;

import fanworld.core.org.lwjglb.game.DummyGame;
import fanworld.core.org.lwjglb.game.HudLoader;
import fanworld.game.client.command.CommandHud;

/** 
* @author byxiaobai
* 类说明 
*/
public class HudManager {
	public static HudManager INSTANCE=new HudManager();
	private HudManager(){}
	private HashMap<String,Hud> hudMap=new HashMap<>();
	private HudLoader hudLoader;
	public void init() {
		hudLoader=DummyGame.INSTANCE.getHudLoader();
		LocationHud locHud=new LocationHud();
		hudLoader.addTextHud(locHud);
		setHud("Location",locHud);
		
		CommandHud cmdHud=new CommandHud();
		hudLoader.addTextHud(cmdHud);
		setHud("Cmd",cmdHud);
	}

	public void addTextHud(String name,TextHud textHud) {
		hudLoader.addTextHud(textHud);
		setHud(name,textHud);
	}
	
	public void setHud(String name,Hud hud) {
		hudMap.put(name.toUpperCase(),hud);
	}
	
	public Hud getHud(String hudName) {
		return hudMap.get(hudName.toUpperCase());
	}
	
	public HudLoader getHudLoader() {
		return this.hudLoader;
	}
}
