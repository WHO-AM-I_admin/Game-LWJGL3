package fanworld.game.client.hud;
/** 
* @author byxiaobai
* 类说明 
*/
public abstract class BasicHud implements Hud{
	private boolean isDisplay=false;
	
	@Override
	public void setDisplay(boolean isDisplay) {
		this.isDisplay=isDisplay;
	}

	@Override
	public boolean isDisplay() {
		return this.isDisplay;
	}
}
