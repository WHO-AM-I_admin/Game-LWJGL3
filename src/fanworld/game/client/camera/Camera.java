package fanworld.game.client.camera;

import fanworld.core.org.lwjglb.engine.graph.CoreCamera;

/** 
* @author byxiaobai
* 类说明 
*/
public class Camera {
	private CoreCamera coreCamera;
	public Camera(CoreCamera coreCamera) {
		this.setCoreCamera(coreCamera);
	}
	public CoreCamera getCoreCamera() {
		return coreCamera;
	}
	public void setCoreCamera(CoreCamera coreCamera) {
		this.coreCamera = coreCamera;
	}
}
