package fanworld.game.client.input.keyboard;
/** 
* @author byxiaobai
* 类说明 
*/
public enum KeyType {
	NORMAL,
	/**
	 * 字母或数字
	 */
	CHAR(932705);
	KeyType(){
		
	}
	private int value;
	KeyType(int value){
		this.value=value;
	}
	public int getValue() {
		return this.value;
	}
}
