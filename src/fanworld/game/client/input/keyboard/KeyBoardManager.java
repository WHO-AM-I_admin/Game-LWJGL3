package fanworld.game.client.input.keyboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fanworld.core.org.lwjglb.engine.Window;
import fanworld.game.client.command.input.CloseCmdGui;
import fanworld.game.client.command.input.InputCmd;
import fanworld.game.client.command.input.OpenCmdGui;

/** 
* @author byxiaobai
* 类说明 
*/
public class KeyBoardManager {
	public static KeyBoardManager INSTANCE=new KeyBoardManager();
	private HashMap<Integer,List<KeyBoard>> keyMap=new HashMap<>();
	private HashMap<Integer,Long> lastTime=new HashMap<>();
	
	public void init() {
		OpenCmdGui openCmdGui=new OpenCmdGui();
		openCmdGui.init(257);
		CloseCmdGui closeCmdGui=new CloseCmdGui();
		closeCmdGui.init(257);
		InputCmd inputCmd=new InputCmd();
		inputCmd.init();
	}
	public void registerKey(int key,KeyBoard handler) {
		if(keyMap.get(key)==null) {
			keyMap.put(key,new ArrayList<>());
		}
		List<KeyBoard> list=keyMap.get(key);
		list.add(handler);
	}
	public void input(Window window) {
		KeyBoardUtil.setWindow(window);
		if(keyMap.isEmpty())return;
		List<KeyBoard> keyBoards=keyMap.get(KeyType.CHAR.getValue());
		if(keyBoards!=null&&(!keyBoards.isEmpty())) {
			for(KeyBoard keyBoard:keyBoards) {
				keyBoard.handle();
			}
		}
		for(int key:keyMap.keySet()) {
			if(key==KeyType.CHAR.getValue())continue;
			if(window.isKeyPressed(key)&&(!isPressed(key))) {
				List<KeyBoard> handlers=keyMap.get(key);
				if(handlers.isEmpty())continue;
				for(KeyBoard handler:handlers) {
					handler.handle();
				}
			}
		}
	}
	public boolean isPressed(int key) {
		Long last=lastTime.get(key);
		long now=System.currentTimeMillis();
		lastTime.put(key, now);
		if(last==null||(now-last>50))return false;
		return true;
	}
}
