package fanworld.game.client;
/** 
* @author byxiaobai
* 类说明 
*/
public class GameData {
	public static final GameData INSTANCE=new GameData();
	
	public boolean isCmdGuiOpen=false;
	public long cmdGuiOpenTime=0;
	
    /**
     * 摄像机移动速度
     */
    public static final float CAMERA_POS_STEP = 0.20f;
	/**
	 * 获取渲染周围区块的范围
	 * @return
	 */
	public double getChunkRenderRange() {
		return 1.5;
	}
}
