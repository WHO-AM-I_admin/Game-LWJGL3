package fanworld.game.world;

import java.util.HashMap;

import fanworld.core.org.lwjglb.engine.items.GameItem;
import fanworld.core.org.lwjglb.game.DummyGame;
import fanworld.game.world.block.Block;
import fanworld.game.world.block.CraftBlock;
import fanworld.game.world.block.blockdata.BlockType;
import fanworld.game.world.block.chunk.Chunk;
import fanworld.game.world.block.chunk.CraftChunk;
import fanworld.game.world.block.utils.LocationUtil;
import fanworld.game.world.entity.Entity;

/** 
* @author byxiaobai
* 默认的世界实现
*/
public class CraftWorld implements World{
	public class MagicData{//TODO SHOULD NOT BE THERE
		public String WORLD_NAME="CRAFT_WORLD";
		public HashMap<Location,Chunk> blockTypeDatas=new HashMap<>();
	}
	private MagicData MAGIC_DATA=new MagicData();
	private CraftNoise craftNoise;
	public CraftWorld() {
		craftNoise=new CraftNoise();
	}
	@Override
	public void addEntity(Entity entity, Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getWorldName() {
		return MAGIC_DATA.WORLD_NAME;//TODO MAGIC
	}

	@Override
	public void addBlock(Block block) {
		Chunk chunk=getChunk(block.getLocation());
		if(chunk==null) {
			chunk=addChunk(block.getLocation());
		}
		chunk.addBlock(block);
	}
	
	@Override
	public Block getBlock(Location location) {
		Chunk chunk=getChunk(location);
		if(chunk==null) {
			chunk=addChunk(location);
		}
		return chunk.getBlock(location);
	}
	
	@Override
	public void setBlock(Location loc,BlockType blockType) {
		CraftBlock block=new CraftBlock(loc,blockType);
    	Chunk nowChunk=this.getChunk(loc);
    	nowChunk.addBlock(block);
    	GameItem newGameItem = block.getGameItem();
    	DummyGame.INSTANCE.addGameItem(newGameItem);
	}

	@Override
	public WorldNoise getNoise() {
		return this.craftNoise;
	}
	
	public Chunk addChunk(Location location) {
		Location loc=LocationUtil.toBlockLocation(new Location(location.getWorld(),location.getX()/16,0,location.getZ()/16));
		MAGIC_DATA.blockTypeDatas.put(loc, new CraftChunk(loc));
		return MAGIC_DATA.blockTypeDatas.get(loc);
	}
	
	@Override
	public Chunk getChunk(Location location) {
		Location loc=LocationUtil.toBlockLocation(new Location(location.getWorld(),location.getX()/16,0,location.getZ()/16));
		return MAGIC_DATA.blockTypeDatas.get(loc);
	}
}
