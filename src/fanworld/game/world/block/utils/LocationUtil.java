package fanworld.game.world.block.utils;

import org.joml.Vector3f;

import fanworld.game.world.Location;
import fanworld.game.world.World;

/** 
* @author byxiaobai
* 类说明 
*/
public class LocationUtil {
	public static Location toBlockLocation(Location location) {
		Location realLocation=location.clone();
		realLocation.setX((int)location.getX());
		realLocation.setY((int)location.getY());
		realLocation.setZ((int)location.getZ());
		return realLocation;
	}
	public static Location toLocation(World world,Vector3f vector) {
		return new Location(world,vector.x,vector.y,vector.z);
	}
	public static Vector3f toVector(Location location) {
		return new Vector3f((float)location.getX(),(float)location.getY(),(float)location.getZ());
	}
}
