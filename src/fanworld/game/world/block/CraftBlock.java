package fanworld.game.world.block;

import fanworld.core.org.lwjglb.engine.items.GameItem;
import fanworld.game.world.Location;
import fanworld.game.world.World;
import fanworld.game.world.block.blockdata.BlockType;

/** 
* @author byxiaobai
* 默认的方块实现 
*/
public class CraftBlock implements Block{
	private Location location;
	private BlockType blockType;
	private GameItem gameItem;
	private float blockScale;
	public CraftBlock(Location location,BlockType blockType,float blockScale) {
		this.location=location;
		this.blockType=blockType;
		
		this.gameItem=new GameItem(blockType.getBlockTypeData().getMesh());
		this.gameItem.setPosition((float)location.getX(), (float)location.getY(), (float)location.getZ());
		gameItem.setScale(blockScale);
	}
	public CraftBlock(Location location,BlockType blockType) {
		this(location,blockType,0.5f);
		World world=location.getWorld();
		world.addBlock(this);
	}
	@Override
	public Location getLocation() {
		return this.location;
	}
	@Override
	public BlockType getBlockType() {
		return this.blockType;
	}
	@Override
	public GameItem getGameItem() {
		return gameItem;
	}
	public void setGameItem(GameItem gameItem) {
		this.gameItem = gameItem;
	}
	public float getBlockScale() {
		return blockScale;
	}
	public void setBlockScale(float blockScale) {
		this.blockScale = blockScale;
		gameItem.setScale(blockScale);
	}
}
