package fanworld.game.world.block.tree;

import fanworld.core.org.lwjglb.engine.graph.Texture;
import fanworld.game.world.block.blockdata.CraftBlockTypeData;

/** 
* @author byxiaobai
* 白烨木
*/
public class BirchLeave extends CraftBlockTypeData{
	private static final float REFLECTANCE=0.1f;
	public BirchLeave() {
		super();
		try {
			Texture texture= new Texture("/textures/trees/birch_leaves.png", 2, 1);
			init(texture,REFLECTANCE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
