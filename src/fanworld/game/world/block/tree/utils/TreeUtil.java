package fanworld.game.world.block.tree.utils;

import fanworld.game.world.Location;
import fanworld.game.world.World;
import fanworld.game.world.block.blockdata.BlockType;

/** 
* @author byxiaobai
* 树处理器
*/
public class TreeUtil {
	private static BlockType logType=BlockType.BIRCH_LOG,leaveType=BlockType.BIRCH_LEAVE;
	/**
	 * 在某个坐标上生成一棵树
	 * @param block
	 */
	public static void createTree(Location loc) {
		World world=loc.getWorld();
		int height=6;
		double orignalY=loc.getY();
		for(int i=0;i<=height-3;i++) {
			loc.setY(orignalY+i);
			world.setBlock(loc, logType);
		}
		for(int i=height-3;i<=height-1;i++) {
			world.setBlock(new Location(loc.getWorld(),loc.getX()-1,orignalY+i,loc.getZ()-1), leaveType);
			world.setBlock(new Location(loc.getWorld(),loc.getX()-1,orignalY+i,loc.getZ()), leaveType);
			world.setBlock(new Location(loc.getWorld(),loc.getX()-1,orignalY+i,loc.getZ()+1), leaveType);
			world.setBlock(new Location(loc.getWorld(),loc.getX(),orignalY+i,loc.getZ()-1), leaveType);
			world.setBlock(new Location(loc.getWorld(),loc.getX(),orignalY+i,loc.getZ()), leaveType);
			world.setBlock(new Location(loc.getWorld(),loc.getX(),orignalY+i,loc.getZ()+1), leaveType);
			world.setBlock(new Location(loc.getWorld(),loc.getX()+1,orignalY+i,loc.getZ()-1), leaveType);
			world.setBlock(new Location(loc.getWorld(),loc.getX()+1,orignalY+i,loc.getZ()), leaveType);
			world.setBlock(new Location(loc.getWorld(),loc.getX()+1,orignalY+i,loc.getZ()+1), leaveType);
		}
		for(int i=height;i<=height;i++) {
			world.setBlock(new Location(loc.getWorld(),loc.getX(),orignalY+i,loc.getZ()), logType);
			
		}
		
		//world.setBlock(new Location(loc.getWorld(),loc.getX()-1,loc.getY()+4,loc.getZ()), newBlock);
		//world.setBlock(new Location(loc.getWorld(),loc.getX()-1,loc.getY()+4,loc.getZ()+1), newBlock);
	}
}
