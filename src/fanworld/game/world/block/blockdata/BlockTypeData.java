package fanworld.game.world.block.blockdata;

import fanworld.core.org.lwjglb.engine.graph.Mesh;

/** 
* @author byxiaobai
* 世界的单位元
*/
public interface BlockTypeData {
	public Mesh getMesh();
}
