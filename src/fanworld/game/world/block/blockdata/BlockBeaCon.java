package fanworld.game.world.block.blockdata;

import fanworld.core.org.lwjglb.engine.graph.Texture;

public class BlockBeaCon extends CraftBlockTypeData{
	private static final float REFLECTANCE=0.1f;
	@SuppressWarnings("unused")
	private static float blockScale = 0.5f;
	
	public BlockBeaCon() throws Exception {
		super();
		Texture texture = new Texture("/textures/beacon.png", 1, 1);
		init(texture,REFLECTANCE);
	}
}
