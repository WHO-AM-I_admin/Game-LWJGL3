package fanworld.game.world.block.blockdata;

import fanworld.core.org.lwjglb.engine.graph.Material;
import fanworld.core.org.lwjglb.engine.graph.Mesh;
import fanworld.core.org.lwjglb.engine.loaders.obj.OBJLoader;
import fanworld.core.org.lwjglb.engine.graph.Texture;

/** 
* @author byxiaobai
* 基本方块
*/
public abstract class CraftBlockTypeData implements BlockTypeData{
	private Mesh mesh;
	private Material material;
	public CraftBlockTypeData(){
	}
	private static float boundingRadius=0;
	/**
	 * 初始化
	 * @param texture
	 * @param reflectance
	 */
	public void init(Texture texture,float reflectance) {
		boundingRadius++;
		//if(boundingRadius==2)return;
		try {
			mesh = OBJLoader.loadMesh("/models/cube.obj",65536);
			//else if(boundingRadius==2)mesh = OBJLoader.loadMesh("/models/cube2.obj",65536);
		} catch (Exception e) {
			e.printStackTrace();
		}//加载方块
		mesh.setBoundingRadius(boundingRadius);
		
		material=new Material(texture, reflectance);
		mesh.setMaterial(material);
	}
	@Override
	public Mesh getMesh() {
		return mesh;
	}
}
