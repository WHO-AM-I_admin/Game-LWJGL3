package fanworld.game.world.block.blockdata;
import fanworld.game.world.block.tree.*;
/** 
* @author byxiaobai
* 方块类型 
*/
public enum BlockType {
	/**
	 * 草方块
	 */
	GRASS(new Grass()),
	/**
	 * 白烨木
	 */
	BIRCH_LOG(new BirchLog()),

	BIRCH_LEAVE(new BirchLeave());
	
	/**
	 * 方块类型数据
	 */
	private BlockTypeData blockTypeData;
	
	private BlockType(BlockTypeData blockTypeData) {
		this.blockTypeData=blockTypeData;
	}

	/**
	 * 方块类型数据
	 * @return
	 */
	public BlockTypeData getBlockTypeData() {
		return blockTypeData;
	}
}
