package fanworld.game.world.block.blockdata;

import fanworld.core.org.lwjglb.engine.graph.Texture;

/** 
* @author byxiaobai
* 类说明 
*/
public class Stone extends CraftBlockTypeData{
	private static final float REFLECTANCE=1.0f;
	public Stone() throws Exception {
		super();
		Texture texture = new Texture("/textures/stone.png");//加载材质
		init(texture,REFLECTANCE);
	}
}
