package fanworld.game.world.block.blockdata;

import fanworld.core.org.lwjglb.engine.graph.Texture;

public class BlockIron extends CraftBlockTypeData{
	private static final float REFLECTANCE=0.1f;
	@SuppressWarnings("unused")
	private static float blockScale = 0.5f;
	
	public BlockIron() throws Exception {
		super();
		Texture texture = new Texture("/textures/iron_block.png", 1, 1);
		init(texture,REFLECTANCE);
	}
	
}
