package fanworld.game.world.entity.ai;

import fanworld.game.world.entity.Entity;

/** 
* @author byxiaobai
* 实体的行动方式
*/
public interface EntityAI {
	/**
	 * 执行AI
	 * @param entity
	 */
	public void doing(Entity entity);
}
