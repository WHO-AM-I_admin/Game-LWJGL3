package fanworld.game.core;

import fanworld.core.org.lwjglb.engine.CoreEngine;
import fanworld.core.org.lwjglb.engine.IGameLogic;
import fanworld.core.org.lwjglb.engine.Window;
import fanworld.core.org.lwjglb.engine.graph.CoreCamera;
import fanworld.core.org.lwjglb.game.DummyGame;
import fanworld.game.client.GameClient;
import fanworld.game.core.version.GameVersion;

/** 
* @author byxiaobai
* 游戏的基本类
*/
public class Game {
	private GameClient gameClient;
	public Game(CoreCamera coreCamera){
		gameClient=new GameClient(coreCamera);
	}
	public GameClient getGameClient() {
		return gameClient;
	}
	public void setGameClient(GameClient gameClient) {
		this.gameClient = gameClient;
	}
	public static class WindowData{
		public static final int WIDTH=1280;
		public static final int HEIGHT=720;
		public static final String TITLE="FanWorld";
	}
	public static void main(String[] args) {
        try {
            boolean vSync = true;
            IGameLogic gameLogic = new DummyGame();
            Window.WindowOptions opts = new Window.WindowOptions();
            opts.cullFace = true;
            opts.showFps = true;
            opts.compatibleProfile = true;
            opts.antialiasing = true;
            opts.frustumCulling = true;
            CoreEngine gameEng = new CoreEngine("Fanworld "+GameVersion.GAME_VERSION, vSync, opts, gameLogic);
            gameEng.start();
        } catch (Exception excp) {
            excp.printStackTrace();
            System.exit(-1);
        }
    }
}
